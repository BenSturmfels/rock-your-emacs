;; Exercise 1: Type this, then run
;; M-x eval-last-sexp (C-x C-e).
(compose-mail "parents@sturm.com.au" "I'm ok.")

;; What is it? What does it do?

;; What would that be in C, PHP, Python, Perl, Ruby etc?











;; Exercise 2: Run M-x eval-expression (M-:),
;; type this, then RET.
(insert "Hi Mum & Dad, I'm ok.")

;; What happens if I misspell insert?

;; Try Eldoc and Show Paren minor modes.
;; M-x eldoc-mode, M-x show-paren-mode











;; Exercise 3: Make this into a reusable function
;; called "mail-parents" and evaluate it (C-x e).
(compose-mail "parents@sturm.com.au" "I'm ok.")
(insert "Hi Mum & Dad, I'm ok.\n\n")
(insert "Love " user-login-name)

;; Run M-x describe-function mail-parents.
;; It's not very useful. Can we fix it?

;; What's \n and what's user-login-name?

;; What happens if I restart Emacs?











;; Exercise 4: Make this function available as
;; the command M-x mail-parents.
(defun mail-parents ()
  "Compose a reply to worried parents."
  (compose-mail "parents@sturm.com.au" "I'm ok.")
  (insert "Hi Mum & Dad, I'm ok.\n\n")
  (insert "Love " user-login-name))

;; Commands? Functions? What's the difference?











;; Exercise 5: Bind our command to the key C-c p.
(global-set-key (kbd "C-c p") 'mail-parents)

;; Great, everyone's happy right?











;; Exercise 6: Write a function to return a
;; kiss or a hug using "random".
(defun kiss-or-hug ()
  "Return a kiss `x' or a hug `o'."
  ;; Your code goes here.
)











;; Exercise 7: Modify mail-parents to add kisses/hugs.
(defun mail-parents ()
  "Compose a reply to worried parents."
  (interactive)
  (compose-mail "parents@sturm.com.au" "I'm ok.")
  (insert "Hi Mum & Dad, I'm ok.\n\n")
  (insert "Love " user-login-name))

(defun kiss-or-hug ()
  "Return a kiss x or a hug o."
  (if (equal (random 2) 0)
      "x"
    "o"))











;; Exercise 8: Put the cursor in the right place
;; to add our own words. Hint: M-x describe-key C-p
(defun mail-parents ()
  "Compose a reply to worried parents."
  (interactive)
  (compose-mail "parents@sturm.com.au" "I'm ok.")
  (insert "Hi Mum & Dad, I'm ok.\n\n")
  (insert "Love " user-login-name " ")
  (dotimes (i 10)
    (insert (kiss-or-hug))))











;; Exercise 9: Copy everything into your .emacs file.
(defun mail-parents ()
  "Compose a reply to worried parents."
  (interactive)
  (compose-mail "parents@sturm.com.au" "I'm ok.")
  (insert "Hi Mum & Dad, I'm ok.\n\n")
  (insert "Love " user-login-name " ")
  (dotimes (i 10) (insert (kiss-or-hug)))
  (forward-line -1))
(defun kiss-or-hug ()
  "Return a kiss x or a hug o."
  (if (equal (random 2) 0) "x" "o"))
(global-set-key (kbd "C-c p") 'mail-parents)











;; Load Eldoc and Show Paren modes when editing
;; Emacs Lisp.
(add-hook 'emacs-lisp-mode-hook
          (lambda ()
            (eldoc-mode)
            (show-paren-mode)))











;; Load a file explicitly:
(load-file "~/.emacs.d/lisp/junk.el")

;; Or update load-path and don't provide path to file.
(add-to-list 'load-path "~/.emacs.d/lisp")
(load "junk")

;; Library code often uses provides/require to load named
;; features only when required and avoid reloading.
(require 'junk)











;; Use loop macro to sum integers 5 to 10.
(require 'cl-lib)
(cl-loop for i
         from 5
         to 10
         sum i)

;; Select only odd numbers from a list.
(cl-remove-if-not 'cl-oddp '(1 2 3 4))
