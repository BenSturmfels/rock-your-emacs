;;; Display

(setq inhibit-startup-screen t)
(setq vc-follow-symlinks t)

;;; Dvorak keyboard

(keyboard-translate ?\C-x ?\C-u)
(keyboard-translate ?\C-u ?\C-x)

;;; Outgoing mail

(setq message-send-mail-function 'message-send-mail-with-sendmail)


(require 'org)
(define-minor-mode presentation-mode
  "Toggle Presentation mode.
Interactively with no argument, this command toggles the mode. A
positive prefix argument enables the mode, any other prefix
disables it. From Lisp, argument omitted or nil enables the mode
`toggle' toggles the state.

When Presentation mode is enabled, the default faces are larger
for easy reading."
  nil
  " Pres"
  :global t
  (cond (presentation-mode
	 (set-face-attribute 'default nil :height 158)
	 (set-face-attribute 'org-meta-line nil :height 0.5 :foreground "gainsboro"))
	(t
	 (set-face-attribute 'default nil :height 98)
	 (set-face-attribute 'org-meta-line nil :height 'unspecified :foreground 'unspecified))))

;;; Load up exercises code in Presentation Mode.
(presentation-mode)
(find-file "~/work/ws/rock-your-emacs/rock-your-emacs.el")



(defun mail-parents ()
  "Compose a reply to worried parents."
  (interactive)
  (compose-mail "parents@sturm.com.au" "I'm ok.")
  (insert "Hi Mum & Dad, I'm ok.\n\n")
  (insert "Love " user-login-name " ")
  (dotimes (i 10) (kiss-or-hug))
  (forward-line -1))
(defun kiss-or-hug ()
  "Return a kiss x or a hug o."
  (if (equal (random 2) 0) "x" "o"))
(global-set-key (kbd "C-c p") 'mail-parents)
