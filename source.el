;; Exercise 1
(compose-mail "parents@sturm.com.au" "I'm still alive.")

;; What would that be in C, PHP, Python, Perl, Ruby etc?






;; Exercise 2:

;; Run M-x eval-expression (M-:), type this, then RET.
(insert "xxoo")

;; What happens if I make a typo?





;; Exercise 3:

;; 3 * 3 in Lisp?


;; 3 * (1 + 1 + 1) in Lisp?









(defun insert-love ()
  "Insert a closing message in an email."
  (interactive)
  (goto-char (point-max))
  (insert "Love from,")
  (newline)
  (insert "Ben xxoo"))

(defun mail-parents ()
  (interactive)
  (compose-mail "parents@sturm.com.au" "Hi, I'm still alive.")
  (save-excursion
    (insert-love)))

(mail-parents)











(defun find-file ()
  (interactive)
  (error "Computer says no."))


















;; Exercise 1: Send a message to user
(message "Hello Perth!")
(message (concat "Hello " user-full-name))

























;; Exercise 2: What happens if you make a typo?
(massage "Hello Perth!")
(message (concat "Hello " user-fool-name))

























;; Exercise 3: Change an existing global variable
(setq user-full-name "Charlie Parker")
(compose-mail)


























;; Exercise 4: Define a new global variable
(setq user-favourite-food "mango")

;; Better still, use `defvar'.
(defvar user-favourite-food "mango"
  "Favourite food of the user logged in.")






















;; Exercise 5: Define a new function
(defun insert-heart ()
  "Insert a heart symbol."
  (interactive)
  (insert "♥"))


























;; Exercise 6: Don't do this, it's silly
(defun find-file ()
  "Break the `find-file' command."
  (interactive)
  (error "Computer says: no."))























;; Exercise 7: A snippet of your .emacs file.
;; Highlight matching parentheses.
(show-paren-mode)


























;; Exercise 8: Loading an external Lisp file
;; In ~/.elisp/dates.el:
(defun insert-iso-date ()
  "Insert the current date in YYYY-MM-DD format."
  (interactive)
  (let ((iso-date (format-time-string
		   "%Y-%m-%d" (current-time))))
    (insert iso-date)))

;; In ~/.emacs:
(add-to-list 'load-path "~/.elisp")
(load "dates")

















;; Exercise 9: Global key binding for `insert-heart'
(global-set-key (kbd "C-c h") 'insert-heart)

























;; Exercise 10: Mode-specific key binding for `insert-iso-date'
(define-key python-mode-map (kbd "C-c .") 'insert-iso-date)

























;; Exercise 11: Using hook functions
;; Enable `eldoc' when using Emacs Lisp mode.
(add-hook 'emacs-lisp-mode-hook 'eldoc-mode)

;; Highlight whitespace when programming
(add-hook 'prog-mode-hook
	  (lambda ()
	    (setq whitespace-style
		  '(face tabs trailing lines-tail))
	    (whitespace-mode)))


























;; Exercise 12: Redact region
(defun redact-region (beg end char)
  "Replace region from BEG to END with character CHAR."
  (interactive "r\ncRedact character: ")
  (save-excursion
    (goto-char beg)
    (while (< beg end)
      (if (eq (char-after) ?\n)
	  (forward-char 1)
	(progn
	  (delete-char 1)
	  (insert char)))
      (setq beg (point)))))















;; Exercise 13: Using Lisp expressions for =interactive=, rather than codes
(defun redact-region (beg end char)
  "Replace region from BEG to END with character CHAR."
  (interactive 
   (list (region-beginning) (region-end)
	 (read-char "Redact character: ")))
  (save-excursion
    (goto-char beg)
    (while (< beg end)
      (if (eq (char-after) ?\n)
	  (forward-char 1)
	(progn
	  (delete-char 1)
	  (insert char)))
      (setq beg (point)))))












;; Exercise 14: Presentation mode with large font
(define-minor-mode presentation-mode
  "Toggle Presentation mode.
  When Presentation mode is enabled, the default faces
  are larger for easy reading."
  nil
  " Pres"
  :global t
  (if presentation-mode
      (set-face-attribute 'default nil :height 158)
    (set-face-attribute 'default nil :height 98)))





















;; Exercise 15: Try debugging with Edebug
(defun greet ()
  (interactive)
  (message (concat "Hello " user-full-name)))
