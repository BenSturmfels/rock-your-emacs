** Defvar for parents email

** Exercise 3: The only example with maths
The action comes first, then the arguments. Looks very strange at first.

#+BEGIN_SRC emacs-lisp :tangle yes
  ;; Exercise 3
  ;; What is 3 * 3 in Lisp?

  ;; What is 3 * (1 + 1 + 1) in Lisp?
#+END_SRC


* Emacs: A beautiful place for Lisping
** Exercise 9: Finding an unknown function
#+BEGIN_SRC emacs-lisp
  (defun mail-parents ()
    (interactive)
    (compose-mail "parents@sturm.com.au" "I'm still alive.")
    (save-excursion
      (insert-closing-love))
    (forward-line 1))
#+END_SRC

 - *demo*: finding out what =C-n= does
 - *demo*: looking up a =forward-line= in docs

** Major modes 
 - /Lisp Interaction mode/ for initial scratch buffer (=lisp-interaction-mode=)
   - =C-j= is =eval-print-last-sexp=
 - /Emacs Lisp mode/ for editing programs (=emacs-lisp-mode=)
 - /Inferior Emacs Lisp mode/ for shell interface (=ielm=)

** Minor modes
 - turn on /Show Paren mode/ (=show-paren-mode=)
 - turn on /Eldoc mode/ (=eldoc-mode=)

** Exercise 10: Enabling =show-paren-mode=
In your =.emacs= file:

#+BEGIN_SRC emacs-lisp
(show-paren-mode)
#+END_SRC

 - =eldoc-mode= later


#+BEGIN_SRC emacs-lisp
  (defun insert-kiss-or-hug ()
    "Insert a hug or a kiss."
    (let ((foo (random 2)))
      (edebug)
      (if (equal foo 0)
          (insert "x")
        (insert "o"))))
#+END_SRC

** Exercise 12: Random hugs in our email
#+BEGIN_SRC emacs-lisp
  (defun insert-love ()
    "Insert a closing message to an email."
    (interactive)
    (insert "Love from,")
    (newline)
    (insert "Ben ")
    (dotimes (i 10)
      (insert-kiss-or-hug)))
#+END_SRC

 - *demo*: Looping in Emacs Lisp Reference Manual

** Summary
 - Emacs is a beautiful environment for programming Emacs Lisp
 - you won't write Emacs Lisp every day, just use help

 - freedom 1 & 2

* Making Emacs your own (25 mins)
** Loading things external to .emacs
 - make a separate directory for your =.el= files
 - add the directory to =load-path=
   - eg. =(add-to-list 'load-path "~/.elisp")=
   - avoid adding = "~/.emacs.d"= to =load-path=
 - =(load "filename")= for regular files
 - =(require 'blah)= if file contains =(provides 'blah)=
   - mechanism to avoid loading a feature twice

** Exercise 8: Loading an external Lisp file
#+BEGIN_SRC emacs-lisp
    ;; In ~/.elisp/mail.el:
    (defun insert-iso-date ()
      "Insert the current date in YYYY-MM-DD format."
      (interactive)
      (let ((iso-date (format-time-string
                       "%Y-%m-%d" (current-time))))
        (insert iso-date)))

    ;; In ~/.emacs:
    (add-to-list 'load-path "~/.elisp")
    (load "dates")
#+END_SRC

** Changing key bindings
 - can bind/rebind any command to any keystroke
 - =C-c [single-letter]= reserved for you
 - =<f5>= to =<f12>= handy too
 - = "\C-ch" = or =[?\C-c ?h]= are internal lisp representations
   - neater to use =kbd=, eg. =(kbd "C-c h")=, =(kbd "<f5>")=
 - global (everywhere) or local (one buffer)

** Global key bindings (everywhere)
 - =global-set-key=:
   - local bindings shadow global (useful)

** Exercise 9: Global key binding for =insert-heart=

`aoeueou'

#+BEGIN_SRC emacs-lisp
  (global-set-key (kbd "C-c h") 'insert-heart)
#+END_SRC

** Summary
 - keep things tidy, use external files
 - bind your favourite things to keys
 - hook functions help you customize Emacs for different modes

** Exercise 10: Turning on Eldoc Mode
#+BEGIN_SRC emacs-lisp :tangle yes
  ;; Exercise 10
  ;; Enable `eldoc' when using Emacs Lisp mode.
  (add-hook 'emacs-lisp-mode-hook 'eldoc-mode)
#+END_SRC


* Blah
my employer would be upset if I didn't mention - automation of all the things

Sticy take and glue, whatever works

Explain symbols vs variable

Update from file:emacs-lisp

* Tutorial overview
** Requirements
1. Copy of talk notes (PDF/HTML/hardcopy)
  - http://sturm.com.au/2014/talks/rock-your-emacs-lca/
2. GNU Emacs 24
3. Emacs Lisp (.el) source files, eg. =emacs24-el=
4. Emacs documentation in Info format, eg. =emacs24-common-non-dfsg=
   - Includes /Emacs Manual/, /Emacs Lisp Intro/ and /Emacs Lisp Reference Manual/

** Tutorial is aimed at
 - frequent Emacs users
 - Emacs Lisp "copy and pasters"
 - some programming experience

** Motivation
 - make Emacs yours
 - a little Lisp makes Emacs more fun
 - barrier to your own customisations is lower than you expect
 - Lisp is an interesting language
 - how has Emacs attained this longevity and love?

** Tutorial format
 - talking + demonstration with Emacs + questions
 - please type along
 - bonus material depending on time
 - function ideas list at back of these notes

** Content outline
 1. some Emacs lisp examples
 2. tools for reading/writing Lisp
 3. making persistent changes
 4. dive deeply into the language and syntax
 5. *5 min break about 2pm*
 6. example customisations/extensions
 7. built-in help and tools

** This is not
 - how to use Emacs
 - memorising lots of lisp --- use help instead
 - heavy on Computer Science
 - preaching Lisp for general purpose programming

* Let's look at some Lisp
** Lisp syntax
 - simple syntax eg. =(+ 1 2)=
 - prefix notation for function and arguments
 - lots of parentheses, but tools help
   - auto-indenting, visual matching


** Example 1: Send a message to user
#+BEGIN_SRC emacs-lisp
  (message "Hello Perth!")
  (message (concat "Hello " user-full-name))
#+END_SRC

** Example 2: What happens if you make a typo?
#+BEGIN_SRC emacs-lisp
  (massage "Hello Perth!")
  (message (concat "Hello " user-fool-name))
#+END_SRC

** Example 3: Change an existing global variable
#+BEGIN_SRC emacs-lisp
  (setq user-full-name "Charlie Parker")
  (compose-mail)
#+END_SRC

** Example 4: Define a new global variable
#+BEGIN_SRC emacs-lisp
  (setq user-favourite-food "mango")

  ;; Better still, use `defvar'.
  (defvar user-favourite-food "mango"
    "Favourite food of the user logged in.")
#+END_SRC

** Example 5: Define a new function
#+BEGIN_SRC emacs-lisp
  (defun insert-heart ()
    "Insert a heart symbol."
    (interactive)
    (insert "♥"))
#+END_SRC

 - *demo*: this is now part of Emacs, via help functions
 - write, eval, test, repeat - tight feedback loop

* How Lisp fits into Emacs
** Emacs is just a Lisp interpreter
 - C for fundamental features/performance
 - mostly Lisp --- most actions run a Lisp function
 - *demo*: =find-file= using Lisp
 - don't worry about all the keybindings
   - interactive commands are an interface tweak
   - binding keys to commands are an interface tweak

** There's no "extension API"
 - Lisp provides full control over Emacs
 - your extensions are indistinguishable from primitives
   - unique and powerful!
 - ultimately can replace built-in functions with your own

** Example 6: Don't do this, it's silly
#+BEGIN_SRC emacs-lisp
  (defun find-file ()
    "Break the `find-file' command."
    (interactive)
    (error "Computer says: no."))
#+END_SRC
     
* Making your extensions persistent
** Emacs initialisation
1. (mine at least) loads =/usr/share/emacs/site-lisp/debian-startup.el=
2. loads =site-run-file= site-wide configuration --- =site-start.el= by default
3. looks for one of =~/.emacs.el=, =~/.emacs= or =~/.emacs.d/init.el=
   - your extensions are usually loaded here

** Example 7: A snippet of your .emacs file.
#+BEGIN_SRC emacs-lisp
  ;; Highlight matching parentheses.
  (show-paren-mode)
#+END_SRC

** Looking after your .emacs
 - put it in version control -- it's a program after all
 - share across computers
 - mine is a symlink to =~/dotfiles/.emacs=

** Resolving .emacs errors
 - whoops, I made a mistake
   - use =emacs --debug-init=
 - *demo*: fixing startup errors

* Overview of Emacs Lisp for programmers
** What's Lisp?
 - Lisp developed in late 1950s
 - originated in "computer science" but very pragmatic
 - simple and elegant syntax
   - code and data in same syntax
   - fully featured
 - rich history, many talented programmers, good writers

** Emacs Lisp language
 - inspired by Maclisp (MIT 1960s) and Common Lisp (standardised 1980s)
   - features excluded/simplified to reduce memory
 - a "Lisp-2", meaning separate namespaces for functions and variables
 - no earmuffs on globals, =*global-var*=

** Lisp vocab 1
 - symbol :: eg. =find-file=
   - name for something --- bound/unbound
   - case-sensitive, but use lowercase
 - symbolic expression/sexp/expression :: eg. =(+ 1 2)=
 - list :: eg. = '(apple orange pear)=
   - made up of nested cons cells, eg. =(cons 1 (cons 2 (cons 3 nil)))=
   - like linked lists
   - =car=, =cdr= --- unfortunate names
 - quote :: take as written, don't eval
   - eg. = 'find-file= or = '(+ 1 2)=
   - shorthand for =(quote (+ 1 2))=

** Lisp vocab 2
 - truth values :: =nil=, =t=
   - everything is true except =nil= or =()=
   - zero is true
   - =nil= and =()= are the same, use in context
 - numbers :: integer =43=, float =3.33=
 - text :: character =?a=, string = "abc" =

** Lisp vocab 3
 - functions ::
   - first-class types
   - lambda anonymous functions
     - eg. =(lambda (x) (1+ x))=
   - *demo*: functions as arguments
     - =(mapcar (lambda (x) (1+ x)) '(1 2 3))=
   - optional arguments with =&optional=
   - variable number of arguments with =&rest=
 - special form :: not many of these
   - builtin, eg. =eval=
   - macro, eg. =dolist=
 - predicates :: boolean functions , eg. =listp= or =buffer-modified-p=

** Some composite data types
 - alist :: association list
   - eg. = '((loc . "Perth") (temp . 39))=
   - substitute for hash table to a point, using =assoc=
   - list of cons cells
 - plist :: property list
 - hash table :: use an alist if you can
 - vector :: use a list if you can
** Emacs-specific vocab
 - interactive :: makes a function available to user interface
 - command :: commands are functions with =(interactive)= applied
 - point :: your cursor location
 - mark :: other end of selection
 - region :: between point and mark
 - marker :: data type of point and mark

** Other language features
 - functions return last expression in body
 - global namespace, no modules
   - use prefix, eg. =rmail-=
 - optional function parameters: =&optional ...=
 - variable number of parameters: =&rest=
 - no named function parameters

** Slightly more obscure features
 - macros: change the language yourself
 - no tail-call recursion optimisation
 - symbols starting with colon, eg. =:group= are "keywords"
   - evaluate to themselves, much like numbers, strings and arrays
 - quasi-quoting: =`(a b ,user-full-name)=
   - or use =(list 'a 'b user-full-name)=

* Tweaking Emacs
** Customize
 - interactive tool for configuring variables
 - *demo*: customize =erc-nick=
 - good for exploring global vars and their values
 - can't define new functions
   - a little constraining

** Manually setting variables
 - *demo*: translate between customization and own Lisp
 - changes made on the fly, no restart (of course)

* More substantial Emacs extensions
** Interactive functions
 - *demo*: =insert-heart= as both interactive and non-interactive
 - generally for side-effect, rather than return value
 - =(interactive)=:
   - makes function available through user interface
   - maintains undo
   - makes Emacs supply or prompt for function arguments
   - suppresses the return value
 - see manual for codes for =interactive=
   - can use Lisp functions instead
** Example 12: Redact region
#+BEGIN_SRC emacs-lisp
  (defun redact-region (beg end char)
    "Replace region from BEG to END with character CHAR."
    (interactive "r\ncRedact character: ")
    (save-excursion
      (goto-char beg)
      (while (< beg end)
        (if (eq (char-after) ?\n)
            (forward-char 1)
          (progn
            (delete-char 1)
            (insert char)))
        (setq beg (point)))))
#+END_SRC

** Example 13: Redact region (without codes)
#+BEGIN_SRC emacs-lisp
    (defun redact-region (beg end char)
      "Replace region from BEG to END with character CHAR."
      (interactive 
       (list (region-beginning) (region-end)
             (read-char "Redact character: ")))
      (save-excursion
        (goto-char beg)
        (while (< beg end)
          (if (eq (char-after) ?\n)
              (forward-char 1)
            (progn
              (delete-char 1)
              (insert char)))
          (setq beg (point)))))
#+END_SRC

** Writing a minor mode
 - use =define-minor-mode= macro and fill in the blanks
 - loads of possibilities, but low barrier to entry

** Example 14: Presentation mode with large font
#+BEGIN_SRC emacs-lisp
  (define-minor-mode presentation-mode
    "Toggle Presentation mode.
  When Presentation mode is enabled, the default faces
  are larger for easy reading."
    nil
    " Pres"
    :global t
    (if presentation-mode
        (set-face-attribute 'default nil :height 158)
      (set-face-attribute 'default nil :height 98)))
#+END_SRC

* Summary
** Further reading

 - [[http://www.gnu.org/software/emacs/emacs-paper.html][EMACS: The Extensible, Customizable Display Editor]] ::
   - high-level discussion of the design of Emacs by Richard Stallman
   - a little out of date, but very interesting


** Conclusion
 - hope brackets are less scary now
   - you understand some lisp
   - comfortable with built-in help
 - text editors are personal
   - distance from thought to change is low here
   - make Emacs suit *your* needs

* Bonus sections
** Bonus: Some tips
  - =eq= for comparing symbols, =equal= for everything else
  - =if=, =when= or =unless= can only contain one expression --- use =progn= or =cond=
  - use =let= to create local variables, take care when using =setq=
  - Lisp and especially Scheme are big on recursion
    - less useful in Emacs due to low default stack limit and no tail-call optimisation
  - Lispers often prefer a functional style, but aren't pedantic
    - Emacs Lisp is all about the side-effects

** Bonus: Common Lisp functions
 - "Lisp" in the web is usually Common Lisp
 - found some code that uses =loop=
 - library =cl-lib= emulates some Common Lisp features, guilt free

#+BEGIN_SRC emacs-lisp
(require 'cl-lib)
(cl-remove-if-not 'cl-oddp '(1 2 3 4))
#+END_SRC

** Bonus: Saving keyboard macros
 - record, save and bind to keys
   - =kmacro-name-last-macro=, then =insert-kbd-macro=
 - *demo*: linkify a list lines
 - no conditionals



** License


