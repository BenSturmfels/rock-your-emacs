pdf:
	emacs --load ~/.emacs --batch --exec '(org-export-as-pdf)' --file rock-your-emacs.org

install-docs:
	sudo apt-get install emacs24-common-non-dfsg

install-elisp-source:
	sudo apt-get install emacs24-el

download-c-source:
	apt-get source emacs24

exercises:
	emacs --batch --exec '(progn (load "org") (org-babel-tangle-file "~/work/ws/rock-your-emacs/rock-your-emacs.org"))'

publish:
	ssh paris.sturm.com.au "mkdir -p /home/web/sturm/htdocs/2015/talks/rock-your-emacs-libreplanet/"
	rsync -avz rock-your-emacs.el rock-your-emacs.pdf index.html paris.sturm.com.au:/home/web/sturm/htdocs/2015/talks/rock-your-emacs-libreplanet/

clean:
	/bin/rm -f *.pdf *.aux *.log *.out *.toc *.tex *.html rock-your-emacs.el

screencast:
	gst-launch-1.0 --eos-on-shutdown ximagesrc ! capsfilter caps=video/x-raw,framerate=3/1,width=1024,height=768 ! queue max-size-bytes=100000000 max-size-time=0 ! jpegenc idct-method=2 quality=90 ! queue max-size-bytes=100000000 max-size-time=0 ! matroskamux name=mux  alsasrc device=hw:0,0 latency-time=100 ! queue max-size-bytes=100000000 max-size-time=0 ! audioconvert ! vorbisenc ! queue max-size-bytes=100000000 max-size-time=0 ! mux. mux. ! queue max-size-bytes=100000000 max-size-time=0 ! filesink sync=false location=/home/ben/tmp/video.mkv

# 3 frames per second MJPEG in Matroska container 1024x768
