(type-with-delay "The quick brown fox jumped over the lazy dog.")


(insert-char "EM DASH")
(insert-char "LATIN SMALL LETTER A")

(defun type-text (text)
  "Auto-type TEXT into the current buffer.
Pauses at the end until you type <Return>."
  (dolist (c (string-to-list text))
    (read-key-sequence-vector)
    (insert c)
    (redisplay))
  (while (not (equal (key-description (read-key-sequence-vector nil)) "RET"))
    ;; Do nothing.
    )
  (newline))

(equal (read-char) (kbd "RET"))



(key-description [13])

(defun foo2 (bar)
  "Blah.
Argument BAR aoeu."
  (let ((j 2))
    (insert "hi")))

(defun random-item (items)
  (nth (random (length items)) items))

(random-item '("x" "o"))
(defun log-command ()
  ;; If there's no window, show the window containing foo
  (with-current-buffer "foo"
    (goto-char 0)
    (insert (prin1-to-string this-command))
    (newline)))

(add-hook 'post-command-hook 'log-command)

(defun pig-latin ()
  (insert "lay"))

(add-hook 'post-self-insert-hook 'pig-latin)

(dolist (c (string-to-list "abc"))
  (insert c))

(car "abc")

(setq x (read-char))

x

(kbd ")
