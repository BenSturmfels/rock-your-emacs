;; Copyright Ben Sturmfels
(defun check-for-copyright ()
  "Checks the current buffer for a copyright notice."
  (when (equal (search-forward "opyright" nil t) nil)
    (when (y-or-n-p "No copyright notice found. Insert one? ")
      (insert-license))))

(defun insert-license ()
  (goto-char 0)
  (let ((beg (point)))
    (insert "Copyright Ben Sturmfels")
    (comment-region beg (point))))


(add-hook 'prog-mode-hook 'check-for-copyright)
